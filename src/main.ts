import type { RUN_MODE } from '@dpapejs/cli/@types'
import Spin from './common/spin'
import { spawn } from 'child_process'
import { extname, join } from 'path'
import {
  SYSDAYA_PATH,
  addPidToCache,
  checkRunPids,
  generateUuid,
  removeDir,
  terminal
} from './common/utils'
import tsTransform from './common/ts-transform'
import { existsSync, mkdirSync } from 'fs'
/**
 * 获取配置文件路径
 */
const getConfigPath = async (outdir: string): Promise<string> => {
  const context = process.cwd()
  const argv = process.argv
  const i = argv.indexOf('--config')
  const defJs = join(context, 'dpape.config.js')
  const defTs = join(context, 'dpape.config.ts')
  if (i >= 0 && process.argv[i + 1]) {
    const argvValue = process.argv[i + 1]
    const value = join(context, argvValue)
    const ext = extname(value)
    const sourcePath = ext
      ? value
      : existsSync(value + '.ts')
      ? value + '.ts'
      : existsSync(value + '.js')
      ? value + '.js'
      : undefined
    if (!sourcePath) {
      throw Error(
        `[ERROR] The configuration file does not exist. ::<${argvValue}>`
      )
    }
    return await tsTransform(value, outdir)
  }
  if (!existsSync(defJs) && !existsSync(defTs)) {
    throw Error(
      `[ERROR] No configuration file found. ::</dpape.config.[js|ts]>`
    )
  }
  return await tsTransform((existsSync(defTs) && defTs) || defJs, outdir)
}

/**
 * 执行命令
 * @param command 命令
 */
const execCmd = async (command: RUN_MODE) => {
  await checkRunPids()
  const DPAPEJS_RUN_PID = generateUuid(16)
  const DPAPEJS_RUN_TEMPDIR = join(SYSDAYA_PATH, DPAPEJS_RUN_PID)
  addPidToCache(DPAPEJS_RUN_PID)
  try {
    const startTime = Date.now()
    terminal.info(
      command === 'build' ? `Starting build App ...` : `Starting run serve ...`
    )
    var spinner: undefined | Spin = undefined
    spinner = new Spin()
    spinner.start()
    !existsSync(DPAPEJS_RUN_TEMPDIR) &&
      mkdirSync(DPAPEJS_RUN_TEMPDIR, { recursive: true })
    const execCmd = process.platform === 'win32' ? 'npx.cmd' : 'npx'
    const execParams = [
      'webpack',
      ...(command === 'serve' ? ['serve'] : []),
      '--config',
      './lib/webpack/runtime.js', // webpack 模块
      '--progress',
      '--color'
    ]
    const DPAPEJS_RUN_MODE = command
    const DPAPEJS_RUN_CONTENT = process.cwd()
    const DPAPEJS_RUN_SERVE_PORT = ''
    const DPAPEJS_RUN_DPACONFIG = await getConfigPath(DPAPEJS_RUN_TEMPDIR)
    const DPAPEJS_ENVS = {
      DPAPEJS_RUN_MODE,
      DPAPEJS_RUN_CONTENT,
      DPAPEJS_RUN_SERVE_PORT,
      DPAPEJS_RUN_PID,
      DPAPEJS_RUN_DPACONFIG,
      DPAPEJS_RUN_TEMPDIR
    }
    const run = spawn(execCmd, execParams, {
      cwd: join(__dirname, '../'),
      stdio: 'pipe',
      env: Object.assign({}, process.env, DPAPEJS_ENVS)
    })
    run.stdout?.on('data', (msg) => {
      spinner && spinner.stop()
      console.log(msg.toString('utf-8'))
    })

    run.stdout?.on('error', (msg) => {
      spinner && spinner.stop()
      console.log(msg.toString())
    })

    run.stderr.on('data', (msg) => {
      spinner && spinner.stop()
      console.log(msg.toString('utf-8'))
    })
    /**
     * The child process exits
     */
    run.on('exit', (code) => {
      spinner && spinner.stop()
      if (command !== 'build' || code !== 0) return
      const msg = `The app has been Build DONE. [Build time:${
        Date.now() - startTime
      } ms]`
      terminal.success(msg)
    })
    const handle = () => {
      removeDir(DPAPEJS_RUN_TEMPDIR)
      terminal.error('Received Exit SIGINT')
      process.exit(0)
    }
    process.on('SIGINT', handle)
    process.on('SIGTERM', handle)
  } catch (error) {
    terminal.error(JSON.stringify(error))
    removeDir(DPAPEJS_RUN_TEMPDIR)
    throw Error(`[ERROR] Run app Fial.`)
  }
}

/**
 * 启动开发服务
 */
export function devServer() {
  execCmd('serve')
}

/**
 * 打包编译
 */
export function packageCompile() {
  execCmd('build')
}
