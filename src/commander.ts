#!/usr/bin/env node
declare const APP_VERSION: string
import { program } from 'commander'
import { RUN_MODE } from '@dpapejs/cli/@types'
import { devServer, packageCompile } from './main'
const argv = process.argv
if (argv[2] === undefined) argv.push('serve')
program.version(APP_VERSION)
program.on('command:*', function () {
  const command = program.args[0] as RUN_MODE
  const commands = ['serve', 'build']
  if (commands.includes(command)) {
    command === 'serve' ? devServer() : packageCompile()
    return
  }
  console.log('See --help for a list of available commands.')
  console.log(`Invalid command: ${command}`, 'ERROR')
  process.exit(0)
})

program.on('--help', function () {
  console.log('Commands:')
  console.log('   serve         Start Run the dev server')
  console.log('   build         Build the project')
})
program.parse(argv)
