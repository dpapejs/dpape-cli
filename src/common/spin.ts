const clearLine = () => {
  if (
    !process ||
    !process.stdout ||
    typeof process.stdout.clearLine !== 'function'
  )
    return
  process.stdout.clearLine(0)
}
class Spin {
  pos: number = 0
  placeholder?: string
  spinner: string = '|/-'
  loop?: NodeJS.Timer
  constructor(placeholder = '') {
    if (!(this instanceof Spin)) return new Spin(placeholder)

    this.pos = 0
    this.placeholder = placeholder || ''
  }

  start() {
    this.loop = setInterval(() => {
      var msg = `\r${this.spinner[this.pos || 0]} ${this.placeholder}`
      process.stdout.write(msg)
      this.pos = ++this.pos % this.spinner.length
    }, 100)
    return this
  }

  stop() {
    clearLine()
    process.stdout.write('\r')
    this.loop && clearInterval(this.loop)
    return this
  }

  update(placeholder: string) {
    clearLine()
    this.placeholder = placeholder + ' '
    return this
  }
}
export default Spin
