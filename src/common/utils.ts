import { execSync } from 'child_process'
import {
  existsSync,
  readFileSync,
  readdirSync,
  rmdirSync,
  statSync,
  unlinkSync,
  writeFileSync
} from 'fs'
import { join } from 'path'

export const SYSDAYA_PATH = join(
  process.env['APPDATA'] || process.env['HOME'] || '',
  '@dpapejs/cli'
)
/**
 * Generate a unique value
 * 生成唯一识别码
 */
export function generateUuid(uint = 8) {
  const s: string[] = []
  var hexDigits = '0123456789abcdefg'
  var i = 0
  Array.from({ length: uint }).forEach(() => {
    const start = Math.floor(Math.random() * 17)
    s[i] = hexDigits.substring(start, start + 1)
    i++
  })
  return s.join('')
}

/**
 * Delete the directory and all subfiles
 */
export function removeDir(dirPath: string): Promise<void> {
  if (!existsSync(dirPath) || !statSync(dirPath).isDirectory()) {
    return Promise.resolve()
  }
  const remove = (dir: string, callback?: Function) => {
    const files = readdirSync(dir)
    let count = 0
    const fn = () => {
      if (count !== files.length) return
      rmdirSync(dir)
      callback && callback()
    }
    if (files.length === 0) {
      fn()
      return
    }
    files.forEach((file) => {
      const curPath = join(dir, file)
      if (statSync(curPath).isDirectory()) {
        remove(curPath, () => {
          count++
          fn()
        })
        return
      }
      unlinkSync(curPath)
      count++
      fn()
    })
  }
  return new Promise((resolve, reject) => {
    try {
      remove(dirPath, () => {
        resolve()
      })
    } catch (error) {
      reject(error)
    }
  })
}

/**
 * 合并对象
 * @param source 源对象
 * @param target 目标对象
 */
export function objectMerge(
  source: Record<string, any>,
  target: Record<string, any>
) {
  if (
    Object.prototype.toString.call(source) !== '[object Object]' ||
    Object.prototype.toString.call(target) !== '[object Object]'
  ) {
    console.warn('[STACK_LOG] OLYM_UTIL: 参数错误')
    return {}
  }
  const result: Record<string, any> = {}
  const sourceKey = Object.keys(source)
  const newKeys = Object.keys(target).filter((key) => !sourceKey.includes(key))
  newKeys.forEach((key) => Object.assign(result, { [key]: target[key] }))
  Object.keys(source).forEach((key) => {
    const newValue = target[key]
    const oldValue = source[key]
    // 对象
    if (
      newValue !== undefined &&
      Object.prototype.toString.call(newValue) === '[object Object]' &&
      Object.prototype.toString.call(oldValue) === '[object Object]'
    ) {
      Object.assign(result, { [key]: objectMerge(oldValue, newValue) })
      return
    }
    Object.assign(result, {
      [key]: newValue !== undefined ? newValue : oldValue
    })
  })
  return result
}

/**
 * 合并对象数组
 * @param source 源对象
 * @param target 目标对象
 */
export function objectListMerge(
  source: Record<string, any>,
  targetList: Record<string, any>[]
) {
  return targetList.reduce(
    (prve, current) => objectMerge(prve, current),
    source
  )
}

/**
 * 美化 CMD 输出样式
 * Beautify CMD output style
 * @param msg 输出信息 / Output message
 * @param type 信息类型 / Message type
 */
function _BeautifyCmd(msg: string, type: consoleType = 'INFO') {
  const maps = {
    INFO: 'blue',
    WARNING: 'orange',
    ERROR: 'red',
    SUCCESS: 'green'
  }
  const fn = (msg: string, type: colorType = 'green') => {
    const colors = {
      green: `32m`,
      red: '31m',
      blue: '34m',
      orange: '30m'
    }
    return `\x1b[${colors[type]}${msg}\x1b[0m`
  }
  const color = maps[type]
  const consoleMsg = fn(type, color as colorType)
  console.log(`[${consoleMsg}] - ${msg}`)
}

export const terminal = {
  info: (msg: string) => _BeautifyCmd(msg, 'INFO'),
  error: (msg: string) => _BeautifyCmd(msg, 'ERROR'),
  success: (msg: string) => _BeautifyCmd(msg, 'SUCCESS'),
  warning: (msg: string) => _BeautifyCmd(msg, 'WARNING')
}

/**
 * 获取进程ID是否运行
 * @param pid 进程ID
 * @returns
 */
export function isRunning(pid: number) {
  if (process.platform === 'win32') {
    const cmd = `tasklist /fi "imagename eq node.exe"`
    const exec = execSync(cmd)
    const pids = exec
      .toString('utf-8')
      .split('\r\n')
      .filter((item) => item.includes('node.exe'))
      .map((item) => item.split(' ').filter((item) => item !== '')[1])
      .filter((pid) => pid !== undefined)
      .map((val) => Number(val))
    return pids.includes(pid)
  }
  const checkPidCmd = `lsof -p ${pid}`
  try {
    execSync(checkPidCmd)
    return true
  } catch (error) {
    return false
  }
}

/**
 * 检测保存PID是否运行，不运行删除缓存文件信息
 */
export async function checkRunPids() {
  const cachePath = join(SYSDAYA_PATH, '.cache.json')
  let cache: Record<number, string> = {}
  if (existsSync(cachePath)) {
    const str = readFileSync(cachePath, 'utf-8')
    cache = JSON.parse(str)
  }
  const pids = Object.keys(cache)
  const updateCache: Record<number, string> = {}
  const removePids: number[] = []
  pids.forEach((val) => {
    const pid = Number(val)
    !isRunning(pid)
      ? removePids.push(Number(pid))
      : Object.assign(updateCache, { [pid]: cache[pid] })
  })
  const fns: Promise<void>[] = removePids.map((pid) =>
    removeDir(join(SYSDAYA_PATH, cache[Number(pid)]))
  )
  await Promise.all(fns)
  writeFileSync(cachePath, JSON.stringify(updateCache), 'utf-8')
}

/**
 * 添加运行进程ID到缓存
 */
export async function addPidToCache(uuid: string) {
  const cachePath = join(SYSDAYA_PATH, '.cache.json')
  const cache: Record<number, string> = {}
  if (existsSync(cachePath)) {
    const str = readFileSync(cachePath, 'utf-8')
    Object.assign(cache, JSON.parse(str))
  }
  const pid = process.pid
  Object.assign(cache, { [pid]: uuid })
  writeFileSync(cachePath, JSON.stringify(cache), 'utf-8')
}
