import vm from 'vm'
export default (code: string, __filename: string) => {
  const cwd = process.env['DPAPEJS_RUN_CONTENT']

  const context = {
    module: {
      exports: {
        default: undefined
      }
    },
    require,
    __dirname: cwd,
    global,
    __filename
  }
  const script = new vm.Script(code)
  vm.createContext(context)
  script.runInContext(context)
  return context.module.exports.default || context.module.exports
}
