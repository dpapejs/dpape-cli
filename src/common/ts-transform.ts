import { build } from 'esbuild'
import { existsSync } from 'fs'
import { join } from 'path'

/**
 * ts 转换
 */
export default async (sourcePath: string, outdir: string): Promise<string> => {
  if (!existsSync(sourcePath)) {
    throw Error(`[ERROR] The file address does not exist. ::<${sourcePath}>`)
  }
  const outfile = join(outdir, 'dpa.js')
  try {
    await build({
      entryPoints: [sourcePath],
      outfile,
      format: 'cjs'
    })
    return outfile
  } catch (error) {
    throw Error(`[ERROR] Build Fail::` + JSON.stringify(error))
  }
}
