import type {
  RUN_MODE,
  ConfigOption,
  CliConfigOptions
} from '@dpapejs/cli/@types'
import { join, resolve } from 'path'
import { objectListMerge } from './common/utils'

export function defineConfig(options: ConfigOption): CliConfigOptions {
  // 默认配置
  const context = resolve(process.env['DPAPEJS_RUN_CONTENT']!)
  const runMode = process.env['DPAPEJS_RUN_MODE'] as RUN_MODE
  const defConfig: CliConfigOptions = {
    alias: {
      '@': join(context, 'src')
    },
    entry: join(context, 'src/app.ts'),
    useTypescipt: true,
    output: join(context, 'dist'),
    outputHtml: true
  }
  const output = options.output
    ? typeof options.output === 'string'
      ? join(context, options.output)
      : options.output
    : defConfig.output
  const plugins = options.plugins || []
  let plugConf: Record<string, any> = {}
  const conf = objectListMerge(defConfig, [options, { output }])
  plugins.forEach((fn) => {
    const pluginsConfig = fn(context, conf, runMode)
    plugConf = objectListMerge(plugConf, [pluginsConfig])
  })
  const result = objectListMerge(conf, [plugConf])
  return result
}
