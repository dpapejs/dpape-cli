import { CliConfigOptions, RUN_MODE } from '@dpapejs/cli/@types'
import getWebpackPlugins from './plugin'
import getWebpackRules from './rules'
import { join, resolve } from 'path'
import { readFileSync } from 'fs'
import runVm from '../common/run-vm'

/**
 * 获取启动服务配置信息
 */
export function getRunServeConfig(appConfig: CliConfigOptions) {
  const { devServerPort, devServerProxy, publicPath } = appConfig
  const port =
    devServerPort || parseFloat(process.env['DPAPEJS_RUN_SERVE_PORT']!)
  const openUrl = `http://localhost:${port}${publicPath || '/'}`
  const result = {
    host: '0.0.0.0',
    hot: true,
    proxy: devServerProxy || {},
    static: {
      publicPath
    },
    client: {
      overlay: {
        errors: true,
        warnings: false
      }
    }
  }
  port
    ? Object.assign(result, { port, open: openUrl })
    : Object.assign(result, { open: true })
  return result
}

/**
 * 获取运行执行模式
 */
export function getExecRunMode(runMode: RUN_MODE) {
  return runMode === 'serve' ||
    (process.argv.includes('--dev') && runMode === 'build')
    ? 'development'
    : 'production'
}

/**
 * 获取webpack运行配置
 */
const getWebpackRuntimeConfig = () => {
  const context = resolve(process.env['DPAPEJS_RUN_CONTENT']!)
  const runMode = process.env['DPAPEJS_RUN_MODE'] as RUN_MODE
  const configPath = process.env['DPAPEJS_RUN_DPACONFIG']!
  const code = readFileSync(configPath, 'utf-8')
  const appConfig = runVm(code, configPath) as CliConfigOptions
  const {
    entry,
    publicPath,
    output,
    testing,
    alias,
    externals,
    webpackPlugins,
    webpackRules
  } = appConfig
  const mode = getExecRunMode(runMode)
  const plugins = getWebpackPlugins(context, appConfig)
  const rules = getWebpackRules(context, appConfig)
  webpackPlugins && plugins.push(...webpackPlugins)
  webpackRules && rules.push(...webpackRules)
  const result = {
    context,
    mode,
    output:
      typeof output === 'string'
        ? {
            path: output,
            filename: testing ? 'js/[name].[chunkhash:8].js' : 'js/[name].js',
            chunkFilename: testing
              ? 'js/[name].[chunkhash:8].js'
              : 'js/[name].js',
            publicPath
          }
        : output,
    entry,
    module: {
      rules
    },
    resolve: {
      extensions: ['.js', '.ts', '.tsx', '.jsx', '.json', '.vue'],
      alias,
      modules: [
        join(context, './node_modules'),
        join(__dirname, '../../node_modules')
      ]
    },
    plugins,
    externals
  }
  runMode === 'serve' &&
    Object.assign(result, { devServer: getRunServeConfig(appConfig) })
  return result
}
export default getWebpackRuntimeConfig()
