/**
 * 获取 webpack 插件配置
 */
import { CliConfigOptions, RUN_MODE } from '@dpapejs/cli/@types'
import { join } from 'path'
import { getAppEnvList, getArgvsParamsValue } from '../utils'

var HtmlWebpackPlugin = require('html-webpack-plugin')
var MiniCssExtractPlugin = require('mini-css-extract-plugin')
var CopyWebpackPlugin = require('copy-webpack-plugin')
import * as webpack from 'webpack'
import { existsSync, readdirSync, statSync } from 'fs'
var ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin')
import { removeDir } from '../common/utils'

/**
 * 检查目录为空目录
 */
const checkDirIsEmpty = (dirPath: string): boolean => {
  if (!existsSync(dirPath)) return false
  const files = readdirSync(dirPath)
  return files.some((file) => {
    const filePath = join(dirPath, file)
    if (statSync(filePath).isDirectory()) {
      return checkDirIsEmpty(filePath)
    }
    return true
  })
}

/**
 * 获取静态文件copy plugi 配置
 */
const getCopyPluginPatterns = (
  data: string | string[],
  context: string,
  output: string
) => {
  const filterFn = (dir: string) => checkDirIsEmpty(join(context, dir))
  const mapFn = (dir: string) => ({
    from: join(context, dir),
    to: output
  })
  const list = Array.isArray(data) ? data : [data]
  return list.filter(filterFn).map(mapFn)
}

export default function (context: string, config: CliConfigOptions) {
  const runMode = process.env['DPAPEJS_RUN_MODE'] as RUN_MODE
  const {
    publicPath,
    testing,
    siteName,
    staticDirectory,
    output,
    outputHtml,
    after,
    useTypescipt
  } = config
  const result: any[] = []
  /**
   * webpack DefinePlugin 环境变量插件
   */
  const envs = getAppEnvList(context, runMode)
  const sysEnvRegx = (key: string) => /^(DPAPEJS_)[\s\S]*/.test(key)
  const appEnvRegx = (key: string) => !/^(DPAPEJS_)[\s\S]*/.test(key)
  const reduceFn = (prve: Record<string, string>, key: string) =>
    Object.assign({}, prve, { [key]: envs[key] })
  const sysEnvs = Object.keys(envs).filter(sysEnvRegx).reduce(reduceFn, {})
  const appEvns = Object.keys(envs).filter(appEnvRegx).reduce(reduceFn, {})
  const packageJsonPath = join(context, 'package.json')
  try {
    if (existsSync(packageJsonPath)) {
      const pgk = require(packageJsonPath)
      Object.assign(sysEnvs, { PACKAGE_VERSION: pgk.version })
    }
  } catch (error) {
    Object.assign(sysEnvs, { PACKAGE_VERSION: '' })
  }
  Object.assign(appEvns, { RUNTIME_MODE: runMode })
  result.push(
    new webpack.DefinePlugin({
      'process.env': JSON.parse(
        `{${Object.keys(appEvns)
          .map((key) => `"${key}":"'${appEvns[key]}'"`)
          .join(',')}}`
      )
    })
  )
  /**
   * webpack html 插件
   */
  if (outputHtml || runMode === 'serve') {
    const defaultHtmlPath = join(__dirname, '../../template/index.html') // 默认 html 模板
    result.push(
      new HtmlWebpackPlugin({
        filename: 'index.html',
        template: defaultHtmlPath,
        inject: true,
        PUBLIC_PATH: publicPath || '',
        SITE_NAME: sysEnvs.DPAPEJS_SITE_NAME || siteName || 'DPAPEJS App'
      })
    )
  }
  if (runMode === 'build') {
    result.push(
      new MiniCssExtractPlugin({
        filename: `assets/css/[name]${testing ? '.[chunkhash:8]' : ''}.css`,
        chunkFilename: `assets/css/[id]${testing ? '.[chunkhash:8]' : ''}.css`
      })
    )
  }
  /**
   * webpack copy-plugin
   */
  staticDirectory &&
    typeof output === 'string' &&
    result.push(
      new CopyWebpackPlugin({
        patterns: getCopyPluginPatterns(staticDirectory, context, output)
      })
    )
  const mode = getArgvsParamsValue(process.argv, '--mode')
  // 防止执行多次
  let afterExec: any = undefined
  const ProgressPlugin = new webpack.ProgressPlugin((percentage) => {
    if (percentage !== 1 || runMode === 'serve') return
    afterExec && clearTimeout(afterExec)
    afterExec = setTimeout(async () => {
      after && after(config, mode)
      const DPAPEJS_RUN_TEMPDIR = process.env['DPAPEJS_RUN_TEMPDIR']
      DPAPEJS_RUN_TEMPDIR && (await removeDir(DPAPEJS_RUN_TEMPDIR))
    }, 300)
  })
  result.push(ProgressPlugin)
  const tsConfigPath = join(context, 'tsconfig.json')
  if (useTypescipt && existsSync(tsConfigPath)) {
    result.push(new ForkTsCheckerWebpackPlugin())
  }
  return result
}
