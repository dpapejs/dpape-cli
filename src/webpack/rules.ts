/**
 * 获取 webpack loader ruls 配置
 */
var MiniCssExtractPlugin = require('mini-css-extract-plugin')

import {
  AppOptionComponentDemand,
  CliConfigOptions,
  RUN_MODE
} from '@dpapejs/cli/@types'
import { existsSync } from 'fs'
import { join } from 'path'
import { getArgvsParamsValue } from '../utils'

export default (context: string, config: CliConfigOptions) => {
  const runMode = process.env['DPAPEJS_RUN_MODE'] as RUN_MODE
  const { componentDemand, useTypescipt, lessGlobalFiles, polyfill } = config
  const result: Record<string, any>[] = []
  /**
   *  babel-loader options
   */
  const demands = []
  if (componentDemand) {
    Array.isArray(componentDemand)
      ? demands.push(...componentDemand)
      : demands.push(componentDemand)
  }
  const mapFn = ({
    libraryName,
    libraryDirectory,
    style
  }: AppOptionComponentDemand) => [
    'import',
    {
      libraryName,
      libraryDirectory,
      style
    },
    libraryName
  ]
  const presets = []
  polyfill && presets.push('@babel/preset-env')
  useTypescipt &&
    polyfill &&
    presets.push([
      '@babel/preset-typescript',
      {
        allExtensions: true,
        isTSX: true,
        jsx: 'preserve',
        jsxFragmentFactory: 'h',
        jsxFactory: 'h'
      }
    ])
  const BabelLoader = {
    test: useTypescipt ? /\.(js|ts|jsx|tsx)$/ : /\.(js|jsx)$/,
    loader: 'babel-loader',
    include: [join(context, 'src')],
    options: {
      presets,
      plugins: demands.map(mapFn)
    }
  }
  result.push(BabelLoader)
  /**
   * ts-loader options
   */
  if (useTypescipt) {
    const tsConfigPath = join(context, 'tsconfig.json')
    const TsLoader = {
      test: /\.(t|j)s$/,
      exclude: /node_modules/,
      use: [
        {
          loader: 'ts-loader',
          options: {
            ...(existsSync(tsConfigPath) ? { configFile: tsConfigPath } : {}),
            appendTsSuffixTo: [/\.vue$/],
            transpileOnly: true
          }
        }
      ]
    }
    result.push(TsLoader)
  }
  /**
   * less-loader options
   */
  const useParams =
    runMode === 'serve' ? ['style-loader'] : [MiniCssExtractPlugin.loader]
  const runtimeEnv = getArgvsParamsValue(process.argv, '--mode')
  const lessGlobalList = lessGlobalFiles
    ? typeof lessGlobalFiles === 'string'
      ? [lessGlobalFiles]
      : lessGlobalFiles
    : []
  const additional: string[] = []
  runtimeEnv && additional.push(`@env: '${runtimeEnv}'`)
  additional.push(...lessGlobalList)
  const lessLoader = {
    test: /\.less$/,
    use: [
      ...useParams,
      'css-loader',
      {
        loader: 'postcss-loader',
        options: {
          postcssOptions: {
            plugins: [
              require('autoprefixer')([
                'last 10 Chrome versions',
                'last 5 Firefox versions',
                'Safari >= 6',
                'ie > 8'
              ])
            ]
          }
        }
      },
      {
        loader: 'less-loader',
        options: {
          lessOptions: {
            javascriptEnabled: true
          },
          additionalData:
            additional.length > 0 ? additional.join(';') + ';' : ''
        }
      }
    ]
  }
  // css loader options
  const cssLoader = {
    test: /\.css$/,
    use: ['style-loader', 'css-loader']
  }
  result.push(...[cssLoader, lessLoader])
  return result
}
