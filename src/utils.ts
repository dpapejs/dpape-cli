import { RUN_MODE } from '@dpapejs/cli/@types'
import { existsSync, readFileSync } from 'fs'
import { join } from 'path'

/**
 * 获取命令行参数值
 * @param argvs 命令行数据 / Command line data
 * @param key 需要获取值的键 / The key to get the value from
 */
export function getArgvsParamsValue(argvs: string[], key: string) {
  const fI = argvs.findIndex((item) => item === key)
  if (fI < 0 || argvs.length - 1 === fI) return
  const result = argvs[fI + 1]
  if (result.indexOf('--') === 0) return
  return result
}

/**
 * 解析 .env 环境变量文件数据
 * @param str .env文件数据
 */
export function parseEnv(str: string) {
  const result: { [key: string]: string } = {}
  const splitList = str
    .split(/(\r\n)|[\r\n]/)
    .filter((value) => !(value === undefined || value === '' || value === null))
  splitList.forEach((item) => {
    const val = item.split('=')
    const key = (val[0] || '').trim()
    const value = (val[1] || '').trim()
    if (key.split('')[0] !== '#' && key !== '') result[key] = value
  })
  return result
}

/**
 * 获取应用环境变量列表
 * @param context 应用运行路径 / Application Running path
 * @returns envResult
 */
export function getAppEnvList(
  context: string,
  runMode: RUN_MODE
): Record<string, string> {
  const result: Record<string, string> = {}
  const envFilePath = join(context, '.env')
  const mode = getArgvsParamsValue(process.argv, '--mode')
  const modeName = mode
    ? mode
    : runMode === 'build'
    ? 'production'
    : 'development'
  const modeEnvPath = join(context, `.env.${modeName}`)
  const env: { [key: string]: string } = {}
  existsSync(envFilePath) &&
    Object.assign(env, parseEnv(readFileSync(envFilePath, 'utf-8')))
  existsSync(modeEnvPath) &&
    Object.assign(env, parseEnv(readFileSync(modeEnvPath, 'utf-8')))
  Object.keys(env).forEach((key) => {
    const value = env[key]
    // 其他自定义环境变量
    result[key] = value
  })
  return result
}

/**
 * 获取本地ipv4地址
 */
export function getIpv4Addrs(): string[] {
  const os = require('os')
  const ifaces = os.networkInterfaces() //网络信息
  const list = Object.keys(ifaces)
    .map((key) => ifaces[key])
    .flat()
    .filter((item) => item.family === 'IPv4' && item.address !== '127.0.0.1')
  return list.map((item) => item.address)
}
