type consoleType = 'INFO' | 'WARNING' | 'ERROR' | 'SUCCESS'
type colorType = 'green' | 'red' | 'blue'
