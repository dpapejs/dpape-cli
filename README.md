# @dpapejs/cli

![npm](https://img.shields.io/npm/v/@dpapejs/cli)
![npm](https://img.shields.io/npm/dm/@dpapejs/cli)
![NPM](https://img.shields.io/npm/l/@dpapejs/cli)

#### Description

🛠️ Webpack5 Tooling for Vue.js Development

#### Scaffolding Your First Dpa Project

> Compatibility Note
>
> Dpape-CLI requires Node.js version >=12.2.0.

Use NPM:

```sh
$ npm init dpa@latest
```

Use Yarn:

```sh
$ yarn create dpa
```

Use PNPM:

```sh
$ pnpm create dpa
```

[Full Docs](http://cli.dpapejs.cn)

#### Change Logs

[Logs](http://cli.dpapejs.cn/change-log.html)

## Default macro processing configuration

```sh
# start run mode
MACRO_[mode name]
# start dev
MACRO_DEV
# build prod
MACRO_PROD
```

### Monitor configuration parameters sent to the server

```ts
interface params {
  errorMessage: string
  message: string
  stack?: string
  source?: string
  line?: number
  column?: number
}
```
