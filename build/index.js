const build = require('esbuild').build
var fs = require('fs')
var path = require('path')

const packagePath = path.join(__dirname, '../src')

/**
 * 别名插件
 * @returns Object
 */
const aliasPlugin = () => {
  return {
    name: 'alias',
    setup(build) {
      build.onLoad({ filter: /\.ts$/ }, async (args) => {
        const source = await fs.promises.readFile(args.path, 'utf8')
        const regx = /from[\s]{1,}[\'\"]@\//g
        const base = path
          .dirname(args.path)
          .split('src')[1]
          .split('\\')
          .filter((v) => v)
        return {
          contents: source.replace(
            regx,
            `from '${base.length === 0 ? '.' : base.map(() => `../`).join('')}/`
          ),
          loader: 'ts'
        }
      })
    }
  }
}

/**
 * 获取编译文件
 * @param {*} url
 * @returns
 */
const getFiles = (url = packagePath) => {
  let result = []
  if (!url || !fs.existsSync(url) || !fs.statSync(url).isDirectory())
    return result
  const files = fs.readdirSync(url)
  files.forEach((filename) => {
    const full = path.join(url, filename)
    if (fs.statSync(full).isDirectory()) {
      result = [...result, ...getFiles(full)]
      return
    }
    const ext = path.extname(full)
    if (['.ts', '.js'].indexOf(ext) < 0) return
    result.push(full)
  })
  return result
}

/**
 * 开发模式
 */
const developmentMode = () => {
  const onWatch = () => {
    let timer = null
    return () => {
      clearTimeout(timer)
      timer = setTimeout(() => {
        try {
          package(false)
        } catch (error) {
          console.log(error)
        }
      }, 500)
    }
  }
  package(false)
  fs.watch(packagePath, { recursive: true }, onWatch())
}

/**
 * 打包编译
 */
const package = async (minify = true) => {
  try {
    const entryPoints = getFiles()
    const { version } = require('../package.json')
    const argvOI = process.argv.findIndex((i) => i === '--o')
    const argvOV = process.argv[argvOI + 1]
    const outdir =
      argvOI >= 0 && argvOV
        ? path.resolve(argvOV)
        : path.join(__dirname, '../lib')
    build({
      entryPoints,
      outdir: outdir,
      format: 'cjs',
      minify,
      define: {
        APP_VERSION: `"${version}"`
      },
      plugins: [aliasPlugin()]
    })
  } catch (error) {
    console.error('[ERROR] 编译失败', error)
  }
}

/**
 * 构建
 */
const buildSrc = () => {
  // TODO 删除旧文件
  package(process.argv.includes('--test'))
}

process.argv.includes('--dev') ? developmentMode() : buildSrc()
