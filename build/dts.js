const { spawnSync } = require('child_process')
const {
  existsSync,
  statSync,
  readdirSync,
  mkdirSync,
  copyFileSync,
  unlinkSync,
  rmdirSync
} = require('fs')
const { join } = require('path')

const cmd = process.platform === 'win32' ? 'npx.cmd' : 'npx'
try {
  spawnSync(cmd, ['tsc'], { stdio: 'ignore', cwd: join(__dirname, '..') })
} catch (error) {
  console.log(error.toString('utf-8'))
  return
}

const dts = join(__dirname, '../typings/src')
const commonDTS = join(dts, 'common')
if (!existsSync(commonDTS) || !statSync(commonDTS).isDirectory()) return

const targetDir = join(__dirname, '../lib/common')

!existsSync(targetDir) && mkdirSync(targetDir, { recursive: true })
copyFileSync(join(dts, 'index.d.ts'), join(__dirname, '../lib/index.d.ts'))
const eachOperDor = (options = {}, type = 'copy') => {
  const remove = type === 'remove'
  const { dir, target, callback } = options
  const files = readdirSync(dir)
  const checkData = { count: 0 }
  const checkDone = () => {
    checkData.count += 1
    if (files.length !== checkData.count) return
    remove && rmdirSync(dir)
    callback && callback()
  }
  if (files.length === 0) {
    remove && rmdirSync(dir)
    callback && callback()
    return
  }
  files.forEach((name) => {
    const fullPath = join(dir, name)
    const targetPath = join(target, name)
    if (statSync(fullPath).isDirectory()) {
      eachOperDor(
        {
          dir: fullPath,
          target: targetPath,
          callback: () => checkDone()
        },
        type
      )
      return
    }
    remove ? unlinkSync(fullPath) : copyFileSync(fullPath, targetPath)
    checkDone()
  })
}
eachOperDor({
  dir: commonDTS,
  target: targetDir,
  callback() {
    eachOperDor({ dir: join(__dirname, '../typings'), target: '' }, 'remove')
  }
})
