// 组件按需加载配置
type styleFunction = (name: string) => string
type customNameFuntion = (name: string, file: Record<string, any>) => string
export declare interface AppOptionComponentDemand {
  libraryName: string
  libraryDirectory?: string
  style?: boolean | 'css' | styleFunction
  styleLibraryDirectory?: string
  customName?: customNameFuntion
  customStyleName?: customNameFuntion
  transformToDefaultImport?: boolean
  camel2DashComponentName?: boolean
}

// 删除控制台输出类型
export declare type RemoveConsoleType = 'log' | 'info' | 'warn'

export declare interface CliConfigOptions extends ConfigOption {
  outputHtml?: boolean // 是否输出html文件
  webpackRules?: Record<string, any>[] // loader 加载配置
  webpackPlugins?: any[]
}

// 应用创建配置项
export declare interface ConfigOption {
  entry?: string | string[] | Record<string, string> // 入口配置
  siteName?: string
  output?: string | Record<string, any> // 构建打包目录
  publicPath?: string // 部署应用包时的基本 URL
  staticDirectory?: string | string[] // 静态目录配置
  sourceMap?: boolean // 是否生成 .map 文件映射
  removeConsole?: RemoveConsoleType[]
  useTypescipt?: boolean // 是否使用 ts
  testing?: boolean // 是否构建测试包
  devServerPort?: number
  devServerProxy?: Record<string, any> // 服务代理配置
  polyfill?: boolean // 是否开启兼容低版本浏览器模式
  alias?: Record<string, string> // 应用内别名路径配置
  macro?: Record<string, boolean> // 宏编译参数
  plugins?: Array<
    (
      context: string,
      config: ConfigOption,
      runMode: RUN_MODE
    ) => CliConfigOptions
  > // 内置插件配置
  componentDemand?: AppOptionComponentDemand | AppOptionComponentDemand[] // 组件按需加载配置项
  externals?: Array<{ [key: string]: string } | Function> // 忽略编译配置
  before?: (config: ConfigOption, mode?: string) => Record<string, any> // 编译之前执行
  after?: (config: ConfigOption, mode?: string) => void // 编译之后执行
  lessGlobalFiles?: string | string[] // less 全局引入
}

export type RUN_MODE = 'serve' | 'build'

export interface RunVmOption {
  ts?: boolean // 是否为ts文件
  cwd?: boolean // 当前执行目录
}
