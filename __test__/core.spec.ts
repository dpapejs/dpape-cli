import { existsSync, mkdirSync, writeFileSync } from 'fs'
import {
  removeDir,
  SYSDAYA_PATH,
  generateUuid,
  isRunning
} from '../src/common/utils'
import { join } from 'path'
test('[func] removeDir', async () => {
  const uuid = generateUuid(32)
  const path = join(SYSDAYA_PATH, uuid)
  !existsSync(path) && mkdirSync(path, { recursive: true })
  writeFileSync(join(path, 'test.txt'), '1111', 'utf-8')
  await removeDir(path)
  expect(existsSync(path)).toBe(false)
})

test('[func] isRunning', () => {
  const runing = isRunning(39328)
  expect(runing).toBe(true)
})
